package cat.epiaedu.damviod.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;

public class GameTest implements ApplicationListener, InputProcessor {
	SpriteBatch batch;
	Texture img;
	TextureAtlas textureAtlas;
	Animation animation;
	float elapsedTime;
	int posx;
	int posy;
	int speedx;
	int speedy;
	Sound mysound;
	Sound mysoundIndle;
	long soundId;
	long soundRunning;
	float maxacc;
	float maxspeed;
	float curracc;
	Boolean running;
	Boolean idle;
	@Override
	public void create () {
		posx = (Gdx.graphics.getWidth()/2)-32;
		posy = (Gdx.graphics.getHeight()/2)-32;
		speedx = 0;
		speedy = 0;
		maxacc = 1;
		curracc = 0;
		maxspeed = 200;
		running = false;
		idle = true;
		batch = new SpriteBatch();
		Gdx.app.log("TEst","create");
		textureAtlas = new TextureAtlas(Gdx.files.internal("atlas/atlas.atlas"));
		animation = new Animation(1/15f, textureAtlas.getRegions());
		Gdx.input.setInputProcessor(this);
		mysound = Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
		mysoundIndle = Gdx.audio.newSound(Gdx.files.internal("engine-idle.wav"));
		soundId=mysoundIndle.play();
		mysoundIndle.setLooping(soundId,true);

	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log("TEst","resize");
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.draw((TextureRegion)animation.getKeyFrame(elapsedTime, true),posx,posy);
		posx += speedx;
		posy += speedy;
		batch.end();
		if(running) {
			if (curracc < maxacc) {
				curracc += 0.01f;
			}
		}
		else if(!running){
				if(curracc>0){
					curracc -= 0.01f;
				}
				else if(curracc<=0){

					if(idle==false){
						mysound.stop();
						soundId=mysoundIndle.play();
						mysoundIndle.setLooping(soundId,true);
						idle=true;
					}

				}
		}

		float pitch = 0.5f + curracc / maxacc * 0.5f;
		mysound.setPitch(soundRunning, pitch);
	}


	@Override
	public void pause() {
		Gdx.app.log("TEst","pause");
	}

	@Override
	public void resume() {
		Gdx.app.log("TEst","resume");
	}

	@Override
	public void dispose () {
		Gdx.app.log("TEst","dispose");
		batch.dispose();
		img.dispose();
	}


	@Override
	public boolean keyDown(int keycode) {
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
			speedx = -20;
		}

		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT	)){
			speedx = 20;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.UP	)){
			speedy = 20;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
			speedy = -20;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
			mysoundIndle.stop();
			soundRunning = mysound.play();
			running = true;
			idle = false;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(!Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			speedx = 0;
		}

		if(!Gdx.input.isKeyPressed(Input.Keys.RIGHT	)){
			speedx = 0;
		}
		if(!Gdx.input.isKeyPressed(Input.Keys.UP)){
			speedy = 0;
		}
		if(!Gdx.input.isKeyPressed(Input.Keys.DOWN)){
			speedy = 0;
		}
		if(!Gdx.input.isKeyPressed(Input.Keys.SPACE)){
			running = false;
			idle = false;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(button==0) {
			posx = screenX-32;
			posy = (Gdx.graphics.getHeight() - screenY)-32;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
